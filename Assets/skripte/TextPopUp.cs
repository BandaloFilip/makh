﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPopUp : MonoBehaviour {

    public Canvas myCanvas;
    public Text[] _text;
   


    IEnumerator OnTriggerEnter2D()
    {
        myCanvas.enabled = true;
        yield return new WaitForSeconds(4.0f);
        myCanvas.enabled = false;
       
    }

   

    //If you want to be more specific to what gets enabled and store it all in one script you can check tags

  
}
