﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfWayPlatform : MonoBehaviour {

    
	// Use this for initialization
	void Start () {
        //thePlayer = GameObject.FindGameObjectsWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("yes");
        var platform = transform.parent;
       
        Physics2D.IgnoreCollision(other.GetComponent<BoxCollider2D>(), platform.GetComponent<BoxCollider2D>());
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //reset jumper's layer to something that the platform collides with
        //just in case we wanted to jump throgh this one
        //other.gameObject.layer = 0;

        //re-enable collision between jumper and parent platform, so we can stand on top again
        Debug.Log("no");
        var platform = transform.parent;
       
        Physics2D.IgnoreCollision(other.GetComponent<BoxCollider2D>(), platform.GetComponent<BoxCollider2D>(), false);
    }
}
