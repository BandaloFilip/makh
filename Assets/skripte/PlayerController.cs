﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float walkSpeed;

    public bool faceingRight;
    //bool za ctrl
    public bool ctrl = false;
    

   


    //jumping 

    public Transform groundCheck;
    public float jumpForce;
    [SerializeField]
    private float groundRadius;

    [SerializeField]
    private LayerMask whatIsGround;
  
    [SerializeField]
    private bool grounded = false;

    Collider2D[] groundCollisions;
    Rigidbody2D _rigidbody;
    Animator _animator;

    void Start()
    {
        
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        faceingRight = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        
        if(grounded && Input.GetButtonDown("Vertical"))
        {
            grounded = false;
            _animator.SetBool("grounded", grounded);
            _rigidbody.AddForce(new Vector3(0, jumpForce, 0));
          
        }
        groundCollisions = Physics2D.OverlapCircleAll(groundCheck.position, groundRadius, whatIsGround);
        if(groundCollisions.Length > 0)
        {
            grounded = true;
        }else
        {
            grounded = false;
           

        }
        _animator.SetBool("grounded", grounded);
        _animator.SetFloat("verticalSpeed", _rigidbody.velocity.y);
        float move = Input.GetAxis("Horizontal");
        _animator.SetFloat("speed", Mathf.Abs(move));
        
            _rigidbody.velocity = new Vector3(move * walkSpeed, _rigidbody.velocity.y, 0);

        if(move > 0 && !faceingRight)
        {
            Flip();
        }
        else if(move < 0 && faceingRight){
            Flip();
        }

      
        
       if (Input.GetButtonDown("Fire1") && ctrl == false)
        {
            ControlFutile();

        }

       
          else if (Input.GetButtonDown("Fire1") && ctrl == true)
        {
            CollorMove();
            // DESTROYAT MRVICU
            // LOAD IDUCI LEVEL VALJDA
        }

       // AKO NETREBA VISE MAKNI SLOGBODNO, OVO JE ZA SVAKI SLUCAJ Debug.Log(ctrl);

    }

    void Flip()
    {
     
        faceingRight = !faceingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void ControlFutile()
    {
        _animator.SetTrigger("controlfutile");
    }

    void CollorMove()
    {
        _animator.SetTrigger("collormove");
        //freezat movment playera
    }

 

    
}
