﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealDmg : MonoBehaviour
{

    public float damage = 1.0f;

    GameObject thePlayer;
    PlayerHealth thePlayerHealth;

    void Start()
    {
        thePlayer = GameObject.FindGameObjectWithTag("Player");
        thePlayerHealth = thePlayer.GetComponent<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            thePlayerHealth.AddDamage(damage);
            Debug.Log("bravo");
           
        }
    }
}