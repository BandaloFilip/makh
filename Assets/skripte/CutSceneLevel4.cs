﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneLevel4 : MonoBehaviour {
    [SerializeField]
    Camera camera1;
    [SerializeField]
    Camera camera2;
    [SerializeField]
    Animator _animator;

    public GameObject _gameobject;
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    IEnumerator OnTriggerEnter2D()
    {

        //Debug.Log("bravo");
        camera1.GetComponent<Camera>().enabled = false;
        camera2.GetComponent<Camera>().enabled = true;
        GameObject.Find("Player").GetComponent<PlayerController>().enabled = false;
        GameObject.Find("Player").GetComponent<Animator>().SetTrigger("cutscene2");
        yield return new WaitForSeconds(9.6f);
        goBack();
        //position gameobject X = 0.746, Y = -0.2
        Destroy(_gameobject);




    }
    void goBack()
    {
        camera1.GetComponent<Camera>().enabled = true;
        camera2.GetComponent<Camera>().enabled = false;
        GameObject.Find("Player").GetComponent<PlayerController>().enabled = true;
    }
}
