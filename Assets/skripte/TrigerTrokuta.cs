﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrigerTrokuta : MonoBehaviour {
    public GameObject triangles;
    public Transform trianglesPosition;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Instantiate(triangles, trianglesPosition);
    }
}
