﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]

public class PlayVideo : MonoBehaviour {

    
    public MovieTexture movie;
    private AudioSource audio;
	
	IEnumerator Start () {
        PlayyVideo();
        yield return new WaitForSeconds(50.0f);
        SceneManager.LoadScene(2);
       
        
    }
	
	// Update is called once per frame
    void PlayyVideo()
    {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        audio = GetComponent<AudioSource>();
        audio.clip = movie.audioClip;
        movie.Play();
        audio.Play();
    }
}
